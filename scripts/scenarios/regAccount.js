import {
  Httpx
} from "https://jslib.k6.io/httpx/0.0.2/index.js";
import {
  randomIntBetween
} from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

import {
  login,
  register
} from "../modules/users.js";
import {
  createCroc,
  checkCroc,
  deleteCroc
} from "../modules/crocs.js";

export default function regAccount() {
  //generate user data
  const EMAIL = `user${randomIntBetween(1, 100000)}@example.com`; // Set your own email;
  const PASSWORD = "superCroc2019";
  const USERNAME = `jmc${randomIntBetween(1, 100000)}`;

  let session = new Httpx({
    baseURL: "https://test-api.k6.io",
  });

  //register and login to newly created user
  register(session, USERNAME, EMAIL, PASSWORD);
  login(session, USERNAME, PASSWORD);

  //check on the created crocs
  checkCroc(session);

  //create a random number of crocs
  session.crocIds = [];
  let crocsToCreate = randomIntBetween(5, 10);
  for (let index = 0; index < crocsToCreate; index++) {
    createCroc(session);
  }
  //check on the created crocs
  checkCroc(session);

  let numOfCrocs = parseInt(session.crocIds.length);
  let crocsToDelete = randomIntBetween(1, numOfCrocs - 1);

  for (let index = 0; index < crocsToDelete; index++) {
    const crocId = session.crocIds[index];
    if (crocId > 0) {
      deleteCroc(session, crocId);
    }
    session.crocIds.splice(index, 1);
  }
}