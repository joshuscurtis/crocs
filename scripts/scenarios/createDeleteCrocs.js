import {
  Httpx
} from "https://jslib.k6.io/httpx/0.0.2/index.js";
import {
  randomIntBetween
} from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

import {
  login,
  register
} from "../modules/users.js";
import {
  checkCroc,
} from "../modules/crocs.js";

export default function createDeleteCrocs() {
  //generate user data
  const EMAIL = `user${randomIntBetween(1, 100000)}@example.com`; // Set your own email;
  const PASSWORD = "superCroc2019";
  const USERNAME = `jmc${randomIntBetween(1, 100000)}`;

  let session = new Httpx({
    baseURL: "https://test-api.k6.io",
  });

  //register and login to newly created user
  register(session, USERNAME, EMAIL, PASSWORD);
  login(session, USERNAME, PASSWORD);
  //check on the created crocs
  checkCroc(session);
}