import {
    textSummary
} from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

export function setup() {
    console.log('helpers.js setup()');
    return {
        foo: 'bar'
    };
}

export function teardown(data) {
    console.log(`helpers.js teardown(${JSON.stringify(data)})`);
}

export function handleSummary(data) {
    console.log('helpers.js handleSummary()');

    return {
        'stdout': textSummary(data, {
            indent: ' ',
            enableColors: true,
        }),
        'output/raw-summary-data.json': JSON.stringify(data, null, 4),
    };
}