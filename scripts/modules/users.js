import { describe } from "https://jslib.k6.io/functional/0.0.3/index.js";
import { check } from "k6";

import {
  randomIntBetween,
  randomItem,
} from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

export function register(session, username, email, password) {
  describe(`Register New User`, (t) => {
    const resp = session.post(`/user/register/`, {
      username: username,
      email: email,
      password: password,
    });
    check(resp, {
      "response code was 201": (res) => res.status == 201,
    });
  });
}

export function login(session, username, password) {
  describe(`Login New User`, (t) => {
    const resp = session.post(`/auth/token/login/`, {
      username: username,
      password: password,
    });
    t.expect(resp.status)
      .as("Auth status")
      .toBeBetween(200, 204)
      .and(resp)
      .toHaveValidJson()
      .and(resp.json("access"))
      .as("auth token")
      .toBeTruthy();

    let authToken = resp.json("access");
    // set the authorization header on the session for the subsequent requests.
    session.addHeader("Authorization", `Bearer ${authToken}`);
    session.userName = username;
  });
}
