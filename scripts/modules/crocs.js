import { describe } from "https://jslib.k6.io/functional/0.0.3/index.js";
import { check } from "k6";

import {
  randomIntBetween,
  randomItem,
} from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

export function checkCroc(session) {
  describe("Check Crocs", (t) => {
    const resp = session.get(`/my/crocodiles/`);
    check(resp, {
      "response code was 200": (res) => res.status == 200,
    });
  });
}

export function deleteCroc(session, crocId) {
  describe("Delete Croc", (t) => {
    console.log("deleteing: " + crocId);
    let resp = session.delete(`/my/crocodiles/${crocId}/`);
    console.log(resp.body);
  });
}

export function createCroc(session) {
  describe("Create New Croc", (t) => {
    let name = randomItem(["Teeth", "Bitey", "Snappy", "Scales"]);
    let gender = randomItem(["M", "F"]);
    let dob =
      randomIntBetween(2000, 2020) +
      "-" +
      randomIntBetween(1, 12) +
      "-" +
      randomIntBetween(1, 28);

    let payload = {
      name: name,
      sex: gender,
      date_of_birth: dob,
    };

    let resp = session.post(`/my/crocodiles/`, payload);

    t.expect(resp.status)
      .as("Croc creation status")
      .toEqual(201)
      .and(resp)
      .toHaveValidJson();
    session.crocIds.push(resp.json("id"));
    session.name = resp.json("name");
    session.dob = resp.json("date_of_birth");
  });
}
