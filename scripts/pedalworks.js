// Creator: k6 Browser Recorder 0.6.2

import { sleep, group } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    // Ramp-up from 1 to 10  virtual users (VUs) in 5m
    { duration: "3m", target: 10 },
    { duration: "5m", target: 10 },

    // RAMP TO 40 VUs and hold for 5m
    { duration: "5m", target: 20 },
    { duration: "5m", target: 20 },

    //RAMP TO 50 VUs and hold for 5m
    { duration: "5m", target: 40 },
    { duration: "10m", target: 40 },

    // Ramp-down from 5 to 0 VUs for 5s
    { duration: "1m", target: 0 },
  ],
};

export default function main() {
  let response;

  const vars = {};

  group("Page-PLP", function () {
    response = http.get("https://www.pedalworks.co.uk/road-bikes/instock", {
      headers: {
        "upgrade-insecure-requests": "1",
        "sec-ch-ua":
          '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"macOS"',
      },
    });
    sleep(5.5);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetMiniBasket",
      null,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetSizeFilterControl",
      '{"controlLocation":"","facetURL":"/road-bikes/instock","searchTerm":"","additionalFacets":""}',
      {
        headers: {
          accept: "application/json, text/javascript, */*; q=0.01",
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetCCartStarbuysControl",
      '{"controlLocation":"/modules/Controls/CCartStarbuys.ascx"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
    sleep(0.5);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetCountryNameAndISOCodesList",
      null,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetAjaxControls",
      '{"baskettemplate":"/modules/controls/CartList.ascx","rewardstemplate":"/modules/controls/CustomerRewards/CustomerRewardsDisplay.ascx","logintemplate":"/modules/controls/clLogin.ascx","campaignID":""}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/StoreGoogleClickID",
      '{"ClientID":"277996623.1615155872"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
    sleep(5.5);
  });

  group("Page-Product-14542", function () {
    response = http.get(
      "https://www.pedalworks.co.uk/bikes/road-bikes/trek-emonda-sl-6-disc-pro-road-bike-in-grey__14542",
      {
        headers: {
          "upgrade-insecure-requests": "1",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    vars["__EVENTARGUMENT1"] = response
      .html()
      .find("input[name=__EVENTARGUMENT]")
      .first()
      .attr("value");

    sleep(1.2);

    response = http.get("https://www.pedalworks.co.uk/wsCitrusStore.asmx/js", {
      headers: {
        "if-modified-since": "Wed, 16 Mar 2022 09:20:49 GMT",
        "sec-ch-ua":
          '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"macOS"',
      },
    });
    sleep(1);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetCountryNameAndISOCodesList",
      null,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/isProductActive",
      '{"prodID":14542}',
      {
        headers: {
          accept: "application/json, text/javascript, */*; q=0.01",
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetFinancePaymentJS",
      '{"PaymentValue":3900,"MonthlyPayment":0,"PaymentCount":0,"Deposit":"0","CreditAmount":3900,"TotalPay":0,"FinanceProduct":"CC12-9.9","ProductName":"Test","Rate":0,"MinDeposit":0,"MaxDeposit":0}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetMiniBasket",
      null,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetAjaxControls",
      `{"baskettemplate":"/modules/controls/CartList.ascx","rewardstemplate":"/modules/controls/CustomerRewards/CustomerRewardsDisplay.ascx","logintemplate":"/modules/controls/clLogin.ascx","campaignID":"${vars["__EVENTARGUMENT1"]}"}`,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/StoreGoogleClickID",
      '{"ClientID":"277996623.1615155872"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetAttributes",
      '{"controlLocation":"/modules/controls/clAttributeControl.ascx","ProductID":14542,"DetailPage":true,"dollar":0,"percentage":0}',
      {
        headers: {
          accept: "application/json, text/javascript, */*; q=0.01",
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetCCartStarbuysControl",
      '{"controlLocation":"/modules/Controls/CCartStarbuys.ascx"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
    sleep(3.7);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/CourierIntegration_GetDeliveryQuote",
      '{"iProductID":14542,"iAttributeID":"5835","iAttributeDetailID":"20757"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetDynamicCurrency",
      '{"controlLocation":"/modules/controls/clCurrency.ascx","SalePrice":3900,"Price":3900,"CurrencyCode":"EUR","productID":14542}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetDynamicCurrency",
      '{"controlLocation":"/modules/controls/clCurrency.ascx","SalePrice":3900,"Price":3900,"CurrencyCode":"EUR,USD,AUD,SEK,NOK,DKK,NZD,JPY","productID":14542}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
    sleep(1.1);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/AddToBasketJSNew",
      '{"iProductID":14542,"iQuantity":1,"iAttributeID":5835,"iAttributeDetailID":20757}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
    sleep(1.4);

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetMiniBasket",
      null,
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );

    response = http.post(
      "https://www.pedalworks.co.uk/wsCitrusStore.asmx/GetCCartStarbuysControl",
      '{"controlLocation":"/modules/Controls/CCartStarbuys.ascx"}',
      {
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "x-requested-with": "XMLHttpRequest",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
        },
      }
    );
  });
}
