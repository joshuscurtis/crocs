export {
  default as myCrocs
}
from "./scenarios/myCrocs.js";
export {
  default as other
}
from "./scenarios/createDeleteCrocs.js";

export {
  setup,
  teardown,
  handleSummary
}
from "./helpers.js";

export let options = {
  thresholds: {
    http_req_failed: ['rate<0.025'],
    http_req_duration: ['p(50) < 1000'],
  },
  scenarios: {
    myCrocs: {
      exec: "myCrocs",
      executor: "ramping-vus",
      startVUs: 0,
      stages: [{
          duration: "1m",
          target: 30,
        },
        {
          duration: "10m",
          target: 30,
        },
        {
          duration: "1s",
          target: 1,
        },
      ],
      gracefulRampDown: "0s",
    },
    createDelete: {
      exec: "other",
      executor: "ramping-vus",
      startVUs: 0,
      stages: [{
          duration: "1m",
          target: 0,
        },
        {
          duration: "1m",
          target: 0,
        },
        {
          duration: "1s",
          target: 1,
        },
      ],
      gracefulRampDown: "0s",
    },
  },
};
